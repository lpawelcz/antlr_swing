tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
String name;
}
prog    : (e+=block | e+=expr | e+=relexpr | d+=decl)* -> prog(name={$e},declarations={$d})
        ;

block   : ^(BRL {locals.enterScope();} (e+=block | e+=expr | d+=decl)* {locals.leaveScope();}) -> block(expr={$e},declarations={$d})
        ;
        
decl    : ^(VAR i1=ID) {name = locals.newSymbol($i1.text);} -> declr(p1={name})
        ;
        catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(AR_ADD  e1=expr e2=expr)                                -> add(p1={$e1.st},p2={$e2.st})
        | ^(AR_SUB e1=expr e2=expr)                                 -> sub(p1={$e1.st},p2={$e2.st})
        | ^(AR_MUL   e1=expr e2=expr)                               -> mul(p1={$e1.st},p2={$e2.st})
        | ^(AR_DIV   e1=expr e2=expr)                               -> div(p1={$e1.st},p2={$e2.st})
        | ^(WRITE i1=ID   e1=expr) {locals.getSymbol($i1.text);}    -> write(p1={$i1.text + "_s" + locals.getSymbolNum($i1.text)},p2={$e1.st})
        | INT                                                       -> getInt(p1={$INT.text})
        | i1=ID                    {locals.getSymbol($i1.text);}    -> getId(p1={$i1.text + "_s" + locals.getSymbolNum($i1.text)})
        | ^(IF {ifstats.enterIfStat();} (c=relexpr t=block e=block) {ifstats.leaveIfStat();})
                                                                    -> if(c={$c.st},t={$t.st},e={$e.st},cnt={ifstats.getIfStat()})
        ;
        catch [RuntimeException ex] {errorID(ex,$i1);}
    
relexpr : ^(REL_E  e1=expr e2=expr)                                 -> relE(p1={$e1.st},p2={$e2.st},cnt={ifstats.getIfStat()})
        | ^(REL_NE  e1=expr e2=expr)                                -> relNE(p1={$e1.st},p2={$e2.st},cnt={ifstats.getIfStat()})                             
        | ^(REL_G  e1=expr e2=expr)                                 -> relG(p1={$e1.st},p2={$e2.st},cnt={ifstats.getIfStat()})
        | ^(REL_GE  e1=expr e2=expr)                                -> relGE(p1={$e1.st},p2={$e2.st},cnt={ifstats.getIfStat()})
        | ^(REL_L  e1=expr e2=expr)                                 -> relL(p1={$e1.st},p2={$e2.st},cnt={ifstats.getIfStat()})
        | ^(REL_LE  e1=expr e2=expr)                                -> relLE(p1={$e1.st},p2={$e2.st},cnt={ifstats.getIfStat()})
        ;   
